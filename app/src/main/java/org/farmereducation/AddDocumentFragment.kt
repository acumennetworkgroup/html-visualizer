package org.farmereducation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputLayout
import org.farmereducation.DocumentProcessState.idle
import org.farmereducation.DocumentProcessState.saving
import org.farmereducation.databinding.FragmentAddDocumentBinding

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class AddDocumentFragment : Fragment() {
    lateinit var bind: FragmentAddDocumentBinding
    private val viewModel: DocumentViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        bind = FragmentAddDocumentBinding.inflate(layoutInflater, container, false)
        return bind.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bind.textInputLayout.editText?.setText(R.string.html)

        with(bind.save) {
            setOnClickListener {
                handleSaveClick()
            }
        }

        bind.listTitle.setOnClickListener {
            viewModel.deleteDocuments()
        }

        setDocumentListView()

        viewModel.getDocumentProcessState().observe(viewLifecycleOwner) {
            bind.save.setText(if (it == saving) R.string.saving else R.string.save)
        }

    }

    private fun Button.handleSaveClick() {
        if (this.id == R.id.save && viewModel.getDocumentProcessState().value == idle) {
            collectInoutAndSave(bind.textInputLayout) {
                setText(R.string.save)
                bind.textInputLayout.editText?.text?.clear()
                makeSuccessToast()
            }
        }
    }

    private fun setDocumentListView() {
        bind.list.apply {
            addItemDecoration(
                BottomSpacingItemDecoration(resources.getDimensionPixelSize(R.dimen.spacing_16_dp))
            )
            adapter = ListAdapter()
                .apply {
                    submitList(listOf())
                    // add data after layout so that animations run
                    viewModel.getDocuments().observe(viewLifecycleOwner) {
                        submitList(it.asDocumentViews())
                    }
                }
        }
    }

    private fun collectInoutAndSave(textInputLayout: TextInputLayout, callback: (String) -> Unit) {
        val input: String = textInputLayout.editText?.text.toString()
        if (input.isNotBlank()) viewModel.addDocument(input, callback)
    }

    private fun makeSuccessToast() {
        Snackbar.make(
            bind.root,
            getString(R.string.document_saved),
            Snackbar.LENGTH_LONG
        ).show()
    }

}