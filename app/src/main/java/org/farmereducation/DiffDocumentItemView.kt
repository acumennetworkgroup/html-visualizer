package org.farmereducation

import androidx.recyclerview.widget.DiffUtil

object DiffDocumentItemView : DiffUtil.ItemCallback<DocumentItemView>() {
    override fun areItemsTheSame(oldItem: DocumentItemView, newItem: DocumentItemView) =
        oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: DocumentItemView, newItem: DocumentItemView) =
        oldItem == newItem
}