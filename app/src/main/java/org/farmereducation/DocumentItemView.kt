package org.farmereducation

import org.htmlparser.core.OfflineDocument
import org.joda.time.DateTime
import java.util.*

data class DocumentItemView(val id: String, val data: String, val updated_at: String)

fun OfflineDocument.entityToItemView(): DocumentItemView {
    val dt = DateTime(updated_at).toLocalDateTime()
    val date = "${dt.dayOfMonth} ${dt.monthOfYear().asShortText.toLowerCase(Locale.ROOT)}" +
            " ${dt.year}" +
            ", ${dt.hourOfDay}H${dt.minuteOfHour}"
    return DocumentItemView(id, data, date)
}

fun List<OfflineDocument>.asDocumentViews() = map { it.entityToItemView() }