package org.farmereducation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.farmereducation.DocumentProcessState.*
import org.htmlparser.core.HtmlLib
import org.htmlparser.core.OfflineDocument

class DocumentViewModel : ViewModel() {
    private var htmlLib: HtmlLib? = null
    private val processState = MutableLiveData(idle)


    fun initLib(htmlLib: HtmlLib) {
        this.htmlLib = htmlLib
    }

    fun getDocumentProcessState() = processState as LiveData<DocumentProcessState>

    fun getDocuments(): LiveData<List<OfflineDocument>> {
        return htmlLib?.getDocuments() ?: MutableLiveData()
    }

    fun addDocument(doc: String, callback: (String) -> Unit) {
        processState.postValue(saving)
        htmlLib?.addDocument(doc) {
            processState.postValue(idle)
            callback(it.data)

        }
    }

    fun deleteDocuments() {
        processState.postValue(deleting)
        htmlLib?.deleteAllDocuments()
        CoroutineScope(Dispatchers.Main).launch {
            delay(1000)
            processState.postValue(idle)
        }
    }
}

enum class DocumentProcessState {
    idle, saving, deleting
}