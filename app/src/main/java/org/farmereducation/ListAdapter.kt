package org.farmereducation

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavDirections
import androidx.navigation.findNavController
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import org.farmereducation.databinding.ListItemBinding

class ListAdapter : ListAdapter<DocumentItemView, ItemViewHolder>(DiffDocumentItemView) {

    private val clickListener = object : DocumentClickListener {
        override fun onClick(view: View, item: DocumentItemView) {
            val action: NavDirections =
                AddDocumentFragmentDirections.actionFirstFragmentToSecondFragment(item.data)
            view.findNavController().navigate(action)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ListItemBinding.inflate(inflater, parent, false)
        return ItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(getItem(position), clickListener)
    }
}


class ItemViewHolder(private val binding: ListItemBinding) : RecyclerView.ViewHolder(binding.root) {
    fun bind(
        itemView: DocumentItemView,
        onClick: DocumentClickListener
    ) {
        binding.item = itemView
        binding.clickHandler = onClick
        binding.executePendingBindings()
    }
}


interface DocumentClickListener {
    fun onClick(view: View, item: DocumentItemView)
}