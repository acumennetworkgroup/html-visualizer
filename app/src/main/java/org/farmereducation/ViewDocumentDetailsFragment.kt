package org.farmereducation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import org.farmereducation.databinding.FragmentDocumentDetailsBinding
import org.htmlparser.core.R
import org.htmlparser.core.rendering.HtmlView
import org.htmlparser.core.rendering.image.HtmlHttpImageGetter

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class ViewDocumentDetailsFragment : Fragment() {

    private lateinit var binding: FragmentDocumentDetailsBinding

    // Type-safe arguments are accessed from the bundle.
    private val args by navArgs<ViewDocumentDetailsFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDocumentDetailsBinding.inflate(layoutInflater, container, false)
        // Inflate the layout for this fragment
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        renderHtmlView(args.data, binding.htmlView)
    }

    private fun renderHtmlView(content: String, view: HtmlView) {
        val imageGetter = HtmlHttpImageGetter(
            view, null, R.drawable.ic_image_place_holder,
            matchParentWidth = false
        )
        view.setHtml(content, imageGetter)

    }
}