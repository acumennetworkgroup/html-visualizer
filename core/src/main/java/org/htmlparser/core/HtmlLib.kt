package org.htmlparser.core

import android.app.Application
import android.content.Context
import android.util.Log
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.htmlparser.core.caching.MediaLinkToFileConverter.processMediaLinks
import org.htmlparser.core.caching.TAG
import org.htmlparser.core.caching.db.HtmlDocumentDatabase
import org.htmlparser.core.caching.db.Repository
import org.htmlparser.core.caching.db.toOfflineDocument
import org.htmlparser.core.parsing.HtmlParser
import org.jsoup.nodes.Document


/**
 * The core public access point to the HTML Parser functionality.
 */
class HtmlLib(private val context: Application) {
    private val database by lazy { HtmlDocumentDatabase.buildDatabase(context) }
    private val repository by lazy { Repository(database) }

    fun addDocument(
        html: String,
        onSuccess: (OfflineDocument) -> Unit
    ) {
        processHtml(context, html, onSuccess)
    }

    private fun processHtml(
        context: Context,
        html: String,
        onSuccess: (OfflineDocument) -> Unit,
    ) {
        val document = HtmlParser.fromHtml(html)
        val completed = document.processMediaLinks(context)
        completed.run {
            showLog(TAG, "Number of tasked completed ${this.size}")
            showLog(TAG, "Media link map  $this")
            val updatedDocument = replaceLinks(this, document.html())
            showLog("PARSED_HTML WITH UPDATED CONTENT", updatedDocument.html())
            val entity = updatedDocument.toOfflineDocument()
            GlobalScope.launch(Dispatchers.IO) { repository.addDocuments(listOf(entity)) }
            onSuccess(entity)
        }
    }

    private fun replaceLinks(
        mediaLinksMap: List<LinkProcessResult>,
        s: String
    ): Document {
        var s1 = s
        with(mediaLinksMap) {
            forEach { link ->
                s1 = s1.replace(link.oldLink, link.newLink /*?: remoteLink*/)
            }
        }
        return HtmlParser.fromHtml(s1)
    }

    fun getDocuments() = repository.getDocuments()

    fun deleteAllDocuments() {
        repository.deleteAll()
    }

}

fun showLog(tag: String, msg: String) {
    Log.i(tag, msg)
}

data class LinkProcessResult(val oldLink: String, val newLink: String)