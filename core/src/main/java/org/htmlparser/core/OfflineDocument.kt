package org.htmlparser.core

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "documents")
data class OfflineDocument(
    @PrimaryKey val id: String,
    val data: String,
    val updated_at: String
)