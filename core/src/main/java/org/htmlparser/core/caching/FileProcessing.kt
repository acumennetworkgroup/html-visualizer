package org.htmlparser.core.caching

import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import android.os.Environment
import androidx.core.content.FileProvider
import org.htmlparser.core.showLog
import java.io.*
import java.util.*


object IO {

    fun Bitmap.writeToFile(
        file: File,
    ): File? {
        val outputStream: FileOutputStream
        return try {
            outputStream = FileOutputStream(file)
            compress(Bitmap.CompressFormat.JPEG, 100, outputStream)
            outputStream.close()
            file
        } catch (error: Exception) {
            error.printStackTrace()
            null
        }
    }


    fun createEmptyFile(
        context: Context, folder: Folder, extension: String
    ): File? {
        val name = "${UUID.randomUUID()}.$extension"

        return try {

            val mFolder = File(
                context.filesDir,
                folder.name
            ) //context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)!!
            if (!mFolder.exists()) {
                mFolder.mkdirs()
            }
            File(mFolder, name)
            // now we can create FileOutputStream and write something to file
        } catch (e: FileNotFoundException) {
            showLog(TAG, "Saving File $name received message failed with : ${e.message}")
            null
        } catch (e: IOException) {
            showLog(TAG, "Saving File $name received message failed with , ${e.message}")
            null
        }
    }


    fun File.getContentUri(context: Context): Uri? {
        return FileProvider.getUriForFile(context, "org.htmlparser.core.fileprovider", this)
    }


    fun localFile(context: Context) {
        //make sure the directories exist.
        val datafiledir: File = context.getExternalFilesDir(null)!!
        datafiledir.mkdirs()
        val datafile = File(datafiledir, "myfiledata.txt")
        //if the file exist, append, else create the file.
        if (datafile.exists()) {
            try {
                val dos = DataOutputStream(FileOutputStream(datafile, true))
                dos.writeUTF("Next line\n")
                dos.close()
                // logger.append("Wrote next line to file\n")
            } catch (e: IOException) {
                e.printStackTrace()
            }
        } else { //file doesn't exist
            try {
                val dos = DataOutputStream(FileOutputStream(datafile)) //no append
                dos.writeUTF("first line\n")
                dos.close()
                // logger.append("Write first line to file\n")
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
        //now read it back.
        try {
            val `in` = DataInputStream(FileInputStream(datafile))
            while (true) try {
                // logger.append(`in`.readUTF())
            } catch (e: EOFException) {  //reach end of file
                `in`.close()
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }


        //now do the same, except use the download directory.
        // logger.append("\nDownload file:\n")
        val dlfiledir: File = context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)!!
        dlfiledir.mkdirs()
        val dlfile = File(dlfiledir, "myfiledl.txt")
        if (dlfile.exists()) {
            try {
                val dos = DataOutputStream(FileOutputStream(dlfile, true))
                dos.writeUTF("2Next line\n")
                dos.close()
                // logger.append("Wrote next line to file\n")
            } catch (e: IOException) {
                e.printStackTrace()
            }
        } else { // file doesn't exist
            try {
                val dos = DataOutputStream(FileOutputStream(dlfile)) //no append
                dos.writeUTF("1first line\n")
                dos.close()
                // logger.append("Write first line to file\n")
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
        //now read it back.
        // logger.append("Now reading it back \n")
        try {
            val `in` = DataInputStream(FileInputStream(dlfile))
            while (true) try {
                // logger.append(`in`.readUTF())
            } catch (e: EOFException) {  //reach end of file
                `in`.close()
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }
}


const val TAG = "HTML IMGs"


data class FileProcessingResult(
    val file: File?,
    val fileSize: MediaDimension?,
    val error: Exception?
)