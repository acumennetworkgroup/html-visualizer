package org.htmlparser.core.caching

/*
* One of these : images, videos, audios
* */
enum class Folder {
    images, videos, audios, documents
}
