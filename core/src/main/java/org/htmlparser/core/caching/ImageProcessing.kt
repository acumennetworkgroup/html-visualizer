package org.htmlparser.core.caching

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import com.bumptech.glide.Glide
import org.apache.commons.io.FilenameUtils
import org.htmlparser.core.caching.IO.writeToFile
import java.io.File


object ImageProcessing {


    fun saveImage(
        context: Context, url: String,
    ): File? {
        val extension = FilenameUtils.getExtension(url)
        var result: FileProcessingResult
        val image: Bitmap = fetchImageOnline(context, url)
        val emptyFile: File? = IO.createEmptyFile(context, Folder.images, extension)

        return if (emptyFile != null) {
            image.writeToFile(emptyFile)
        } else null
    }


    private fun fetchImageOnline(
        context: Context,
        url: String
    ): Bitmap {
        return try {
            Glide.with(context)
                .asBitmap()
                .load(url)
                .error(android.R.drawable.stat_notify_error)
                .submit()
                .get()
        } catch (e: Exception) {
            BitmapFactory.decodeResource(context.resources, android.R.drawable.stat_notify_error)
        }
    }

    fun getImageSize(imageUriPath: String): MediaDimension {
        val uri: Uri = Uri.parse(imageUriPath)
        val path = uri.path ?: ""
        return path.run {
            File(path).absolutePath
            BitmapFactory.decodeFile(path, options)
            val width = options.outWidth
            val height = options.outHeight
            MediaDimension(width, height)
        }
    }


    private val options by lazy {
        BitmapFactory.Options().apply {
            inJustDecodeBounds = true
        }
    }

}

data class MediaDimension(val width: Int, val height: Int)

data class ImageProcessingResult(
    val bitmap: Bitmap?,
    val size: MediaDimension?,
    val error: Exception?
)