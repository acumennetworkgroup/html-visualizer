package org.htmlparser.core.caching

import android.content.Context
import kotlinx.coroutines.*
import org.htmlparser.core.LinkProcessResult
import org.htmlparser.core.showLog
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element

object MediaLinkToFileConverter {

    fun Document.processMediaLinks(
        context: Context,
    ): List<LinkProcessResult> = runBlocking {
        getElementsByTag("img").localizeMediaSources(context).awaitAll()
    }

    /*
    * Fetches media image sources and get a copy of the images on device.
    * Also update the HTML doc with the new local images's paths.
    * All this done in coroutines to make the process the fastest possible.
    * */
    private fun List<Element>.localizeMediaSources(
        context: Context
    ): List<Deferred<LinkProcessResult>> =
        (0 until size).map { index ->

            GlobalScope.async {
                // iterate through a list of children nodes to retrieve 'src' attr, in parallel mode rather
                // than sequentially
                collectImageResources(index, context).apply {
                    showLog(TAG, "Source: ${this.oldLink}, local path: ${this.newLink}\n")
                }
            }
        }

    private fun List<Element>.collectImageResources(
        index: Int,
        context: Context
    ): LinkProcessResult {
        val source: String = this[index].getImageTagValue()
        showLog(TAG, "Img source $index : $source")
        val filePath: String = ImageProcessing.saveImage(
            context, source
        )?.absolutePath ?: source

        return LinkProcessResult(source, filePath)
    }

    private fun Element.getImageTagValue(): String = attr("src")
}