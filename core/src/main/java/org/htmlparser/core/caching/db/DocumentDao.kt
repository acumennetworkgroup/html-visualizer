package org.htmlparser.core.caching.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import org.htmlparser.core.OfflineDocument

@Dao
interface DocumentDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun add(documents: List<OfflineDocument>)

    @Query("SELECT * FROM documents")
    fun getDocuments(): LiveData<List<OfflineDocument>>

    @Query("DELETE FROM documents")
    suspend fun deleteAll()
}