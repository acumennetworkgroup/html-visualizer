package org.htmlparser.core.caching.db

import android.app.Application
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import org.htmlparser.core.OfflineDocument

/**
 * The [Room] database for this app.
 */
@Database(
    entities = [OfflineDocument::class],
    version = 1,
    exportSchema = false
)
abstract class HtmlDocumentDatabase : RoomDatabase() {
    abstract fun documentDao(): DocumentDao

    companion object {
        private const val databaseName = "html-documents-db"

        fun buildDatabase(context: Application): HtmlDocumentDatabase {
            return Room.databaseBuilder(context, HtmlDocumentDatabase::class.java, databaseName)
                .fallbackToDestructiveMigration()
                .build()
        }
    }
}