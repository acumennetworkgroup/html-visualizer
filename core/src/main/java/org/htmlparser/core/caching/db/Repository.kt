package org.htmlparser.core.caching.db

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import org.htmlparser.core.OfflineDocument
import org.joda.time.DateTime
import org.jsoup.nodes.Element
import java.util.*

internal class Repository(private val database: HtmlDocumentDatabase) {

    suspend fun addDocuments(documents: List<OfflineDocument>) =
        database.documentDao().add(documents)

    fun getDocuments() = database.documentDao().getDocuments()
    fun deleteAll() {
        GlobalScope.async { database.documentDao().deleteAll() }
    }
}

fun Element.toOfflineDocument(): OfflineDocument {
    return OfflineDocument(
        id = UUID.randomUUID().toString(),
        data = html(),
        updated_at = DateTime.now().toString()
    )
}