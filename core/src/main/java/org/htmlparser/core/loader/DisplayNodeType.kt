package org.htmlparser.core.loader

enum class DisplayNodeType {
    image, video, audio
}
