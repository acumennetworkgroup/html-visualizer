package org.htmlparser.core.parsing

import android.text.Html.ImageGetter
import android.text.Spanned
import org.htmlparser.core.OnClickATagListener
import org.htmlparser.core.parsing.HtmlParser.fromHtml
import org.htmlparser.core.rendering.table.ClickableTableSpan
import org.htmlparser.core.rendering.table.DrawTableLinkSpan

object HtmlFormatter {
    fun formatHtml(builder: HtmlFormatterBuilder): Spanned? {
        return formatHtml(
            builder.html, builder.imageGetter, builder.clickableTableSpan,
            builder.drawTableLinkSpan, object : TagClickListenerProvider {
                override fun provideTagClickListener(): OnClickATagListener? {
                    return builder.onClickATagListener
                }
            }, builder.indent,
            builder.isRemoveTrailingWhiteSpace
        )
    }

    fun formatHtml(
        html: String?,
        imageGetter: ImageGetter?,
        clickableTableSpan: ClickableTableSpan?,
        drawTableLinkSpan: DrawTableLinkSpan?,
        tagClickListenerProvider: TagClickListenerProvider?,
        indent: Float,
        removeTrailingWhiteSpace: Boolean
    ): Spanned? {
        var html = html
        val htmlTagHandler = HtmlTagHandler()
        htmlTagHandler.setClickableTableSpan(clickableTableSpan)
        htmlTagHandler.setDrawTableLinkSpan(drawTableLinkSpan)
        htmlTagHandler.setOnClickATagListenerProvider(tagClickListenerProvider)
        htmlTagHandler.setListIndentPx(indent)

        html = htmlTagHandler.overrideTags(html)
        return if (removeTrailingWhiteSpace) {
            removeHtmlBottomPadding(
                fromHtml(
                    html!!,
                    imageGetter!!,
                    WrapperContentHandler(htmlTagHandler)
                )
            )
        } else {
            fromHtml(html!!, imageGetter!!, WrapperContentHandler(htmlTagHandler))
        }
    }

    /**
     * Html.fromHtml sometimes adds extra space at the bottom.
     * This methods removes this space again.
     * See https://github.com/SufficientlySecure/html-textview/issues/19
     */
    private fun removeHtmlBottomPadding(text: Spanned?): Spanned? {
        var text = text ?: return null
        while (text.isNotEmpty() && text[text.length - 1] == '\n') {
            text = text.subSequence(0, text.length - 1) as Spanned
        }
        return text
    }

    interface TagClickListenerProvider {
        fun provideTagClickListener(): OnClickATagListener?
    }
}