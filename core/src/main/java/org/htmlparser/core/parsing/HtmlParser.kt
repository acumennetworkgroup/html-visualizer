package org.htmlparser.core.parsing

import android.os.Build
import android.text.Html
import android.text.Spanned
import org.jsoup.Jsoup
import org.jsoup.nodes.Document

object HtmlParser {

    fun fromHtml(
        source: String, imageGetter: Html.ImageGetter,
        tagHandler: Html.TagHandler
    ): Spanned? {

        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(
                source, Html.FROM_HTML_MODE_LEGACY,
                imageGetter, tagHandler
            )
        } else Html.fromHtml(source, imageGetter, tagHandler)
    }

    fun fromHtml(html: String): Document = Jsoup.parse(html) //.body()
}