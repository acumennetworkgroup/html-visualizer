package org.htmlparser.core.rendering

import android.content.Context
import android.text.Html.ImageGetter
import android.text.Spannable
import android.text.Spanned
import android.text.style.QuoteSpan
import android.util.AttributeSet
import androidx.annotation.RawRes
import androidx.core.content.ContextCompat
import androidx.core.text.PrecomputedTextCompat
import androidx.core.widget.TextViewCompat
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.htmlparser.core.OnClickATagListener
import org.htmlparser.core.R
import org.htmlparser.core.parsing.HtmlFormatter
import org.htmlparser.core.rendering.table.ClickableTableSpan
import org.htmlparser.core.rendering.table.DrawTableLinkSpan
import org.htmlparser.core.rendering.text.ElegantQuoteSpan
import org.htmlparser.core.rendering.text.JellyBeanSpanFixTextView
import java.io.InputStream
import java.lang.ref.WeakReference
import java.util.*

class HtmlView : JellyBeanSpanFixTextView {
    var blockQuoteBackgroundColor = ContextCompat.getColor(context, R.color.White)
    var blockQuoteStripColor = ContextCompat.getColor(context, R.color.black)
    var blockQuoteStripWidth = 10f
    var blockQuoteGap = 20f
    private var clickableTableSpan: ClickableTableSpan? = null
    private var drawTableLinkSpan: DrawTableLinkSpan? = null
    private var onClickATagListener: OnClickATagListener? = null
    private var indent = 24.0f // Default to 24px.
    private var removeTrailingWhiteSpace = false

    val mParams by lazy { TextViewCompat.getTextMetricsParams(this) }
    val ref by lazy { WeakReference(this) }


    constructor(context: Context?, attrs: AttributeSet?, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    )

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?) : super(context)

    /**
     * @see HtmlView.setHtml
     */
    fun setHtml(@RawRes resId: Int) {
        setHtml(resId, null)
    }

    /**
     * @see HtmlView.setHtml
     */
    fun setHtml(html: String) {
        setHtml(html, null)
    }

    /**
     * Loads HTML from a raw resource, i.e., a HTML file in res/raw/.
     * This allows translatable resource (e.g., res/raw-de/ for german).
     * The containing HTML is parsed to Android's Spannable format and then displayed.
     *
     * @param resId       for example: R.raw.help
     * @param imageGetter for fetching images. Possible ImageGetter provided by this library:
     * HtmlLocalImageGetter and HtmlRemoteImageGetter
     */
    private fun setHtml(@RawRes resId: Int, imageGetter: ImageGetter?) {
        val inputStreamText = context.resources.openRawResource(resId)
        setHtml(convertStreamToString(inputStreamText), imageGetter)
    }

    /**
     * Parses String containing HTML to Android's Spannable format and displays it in this TextView.
     * Using the implementation of Html.ImageGetter provided.
     *
     * @param html        String containing HTML, for example: "**Hello world!**"
     * @param imageGetter for fetching images. Possible ImageGetter provided by this library:
     * HtmlLocalImageGetter and HtmlRemoteImageGetter
     */
    fun setHtml(html: String, imageGetter: ImageGetter?) {
        val styledText = HtmlFormatter.formatHtml(
            html, imageGetter, clickableTableSpan, drawTableLinkSpan,
            null, indent, removeTrailingWhiteSpace
        )
        if (styledText != null) {
            replaceQuoteSpans(styledText)

            GlobalScope.launch(Dispatchers.Default) {
                // worker thread
                val pText = PrecomputedTextCompat.create(styledText, mParams)
                TextViewCompat.setPrecomputedText(this@HtmlView, pText)

                GlobalScope.launch(Dispatchers.Main) {
                    // main thread
                    ref.get()?.let { textView ->
                        textView.text = pText
                        //  textView.invalidate()
                    }
                }
            }
        }
        text = styledText

        // make links work
        movementMethod = LocalLinkMovementMethod.instance
    }

    /**
     * The Html.fromHtml method has the behavior of adding extra whitespace at the bottom
     * of the parsed HTML displayed in for example a TextView. In order to remove this
     * whitespace call this method before setting the text with setHtml on this TextView.
     *
     * @param removeTrailingWhiteSpace true if the whitespace rendered at the bottom of a TextView
     * after setting HTML should be removed.
     */
    fun setRemoveTrailingWhiteSpace(removeTrailingWhiteSpace: Boolean) {
        this.removeTrailingWhiteSpace = removeTrailingWhiteSpace
    }

    fun setClickableTableSpan(clickableTableSpan: ClickableTableSpan?) {
        this.clickableTableSpan = clickableTableSpan
    }

    fun setDrawTableLinkSpan(drawTableLinkSpan: DrawTableLinkSpan?) {
        this.drawTableLinkSpan = drawTableLinkSpan
    }

    fun setOnClickATagListener(onClickATagListener: OnClickATagListener?) {
        this.onClickATagListener = onClickATagListener
    }

    /**
     * Add ability to increase list item spacing. Useful for configuring spacing based on device
     * screen size. This applies to ordered and unordered lists.
     *
     * @param px pixels to indent.
     */
    fun setListIndentPx(px: Float) {
        indent = px
    }

    private fun replaceQuoteSpans(spanned: Spanned) {
        val spannable = spanned as Spannable
        val quoteSpans = spannable.getSpans(0, spannable.length - 1, QuoteSpan::class.java)
        for (quoteSpan in quoteSpans) {
            val start = spannable.getSpanStart(quoteSpan)
            val end = spannable.getSpanEnd(quoteSpan)
            val flags = spannable.getSpanFlags(quoteSpan)
            spannable.removeSpan(quoteSpan)
            spannable.setSpan(
                ElegantQuoteSpan(
                    blockQuoteBackgroundColor,
                    blockQuoteStripColor,
                    blockQuoteStripWidth,
                    blockQuoteGap
                ),
                start,
                end,
                flags
            )
        }
    }

    companion object {
        const val TAG = "HtmlView"
        const val DEBUG = false

        /**
         * http://stackoverflow.com/questions/309424/read-convert-an-inputstream-to-a-string
         */
        private fun convertStreamToString(`is`: InputStream): String {
            val s = Scanner(`is`).useDelimiter("\\A")
            return if (s.hasNext()) s.next() else ""
        }
    }
}