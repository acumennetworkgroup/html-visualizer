package org.htmlparser.core.rendering

import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.util.Log
import android.view.View
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import org.htmlparser.core.rendering.image.HtmlHttpImageGetter
import org.htmlparser.core.rendering.image.UrlDrawable
import java.io.*
import java.lang.ref.WeakReference
import java.net.URI
import java.net.URL


class ImageGetterCoroutinesTask(
    private val d: UrlDrawable,
    private val imageGetter: HtmlHttpImageGetter,
    private val container: View,
    private val matchParentWidth: Boolean = false,
    private val compressImage: Boolean = false,
    private val qualityImage: Int = 50
) {

    private val drawableReference: WeakReference<UrlDrawable> = WeakReference(d)
    private val imageGetterReference: WeakReference<HtmlHttpImageGetter> =
        WeakReference(imageGetter)
    private val containerReference: WeakReference<View> = WeakReference(container)
    private val resources: WeakReference<Resources> = WeakReference(container.resources)
    private var source: String? = null
    private var scale = 0f


    fun execute(source: String) {
        GlobalScope.async {
            val drawable = fetchMedia(source)
            drawMedia(drawable)
        }
    }

    private fun fetchMedia(source: String): Drawable? {
        this.source = source
        return if (resources.get() != null) {
            return if (compressImage) {
                fetchCompressedDrawable(resources.get(), source)
            } else {
                fetchDrawable(resources.get(), source)
            }
        } else null
    }

    private fun drawMedia(drawable: Drawable?) {
        if (drawable == null) {
            Log.w(HtmlView.TAG, "Drawable result is null! (source: $source)")
            return
        }
        val urlDrawable = drawableReference.get() ?: return
        // set the correct bound according to the result from HTTP call
        // set the correct bound according to the result from HTTP call
        urlDrawable.setBounds(
            0, 0, (drawable.intrinsicWidth * scale).toInt(),
            (drawable.intrinsicHeight * scale).toInt()
        )

        // change the reference of the current drawable to the result from the HTTP call

        // change the reference of the current drawable to the result from the HTTP call
        urlDrawable.drawable = drawable

        val imageGetter: HtmlHttpImageGetter = imageGetterReference.get() ?: return
        // redraw the image by invalidating the container
        imageGetter.container.invalidate()
        // re-set text to fix images overlapping text
        imageGetter.container.text = imageGetter.container.text
    }


    /**
     * Get the Drawable from URL
     */
    private fun fetchDrawable(res: Resources?, urlString: String): Drawable? {
        return try {


            val `is` = fetch {
                val f = File(urlString)
                if (f.exists()) fetchFromFile(f) else fetchOnline(urlString)
            }

            val drawable: Drawable = BitmapDrawable(res, `is`)
            scale = getScale(drawable)
            drawable.setBounds(
                0, 0, (drawable.intrinsicWidth * scale).toInt(),
                (drawable.intrinsicHeight * scale).toInt()
            )
            drawable
        } catch (e: Exception) {
            null
        }
    }

    /**
     * Get the compressed image with specific quality from URL
     */
    private fun fetchCompressedDrawable(res: Resources?, urlString: String): Drawable? {
        return try {
            val `is`: InputStream? = fetch {
                val f = File(urlString)
                if (f.exists()) fetchFromFile(f) else fetchOnline(urlString)
            }
            val original = BitmapDrawable(res, `is`).bitmap
            val out = ByteArrayOutputStream()
            original.compress(Bitmap.CompressFormat.JPEG, qualityImage, out)
            original.recycle()
            `is`?.close()
            val decoded = BitmapFactory.decodeStream(ByteArrayInputStream(out.toByteArray()))
            out.close()
            scale = getScale(decoded)
            val b = BitmapDrawable(res, decoded)
            b.setBounds(
                0, 0, (b.intrinsicWidth * scale).toInt(),
                (b.intrinsicHeight * scale).toInt()
            )
            b
        } catch (e: Exception) {
            null
        }
    }

    private fun getScale(bitmap: Bitmap): Float {
        val container = containerReference.get() ?: return 1f
        val maxWidth = container.width.toFloat()
        val originalDrawableWidth = bitmap.width.toFloat()
        return maxWidth / originalDrawableWidth
    }

    private fun getScale(drawable: Drawable): Float {
        val container = containerReference.get()
        if (!matchParentWidth || container == null) {
            return 1f
        }
        val maxWidth = container.width.toFloat()
        val originalDrawableWidth = drawable.intrinsicWidth.toFloat()
        return maxWidth / originalDrawableWidth
    }


    fun fetch(function: () -> InputStream?): InputStream? {
        return function()
    }

    @Throws(IOException::class)
    private fun fetchOnline(urlString: String): InputStream? {
        val url: URL?
        val imageGetter = imageGetterReference.get()
        url = if (imageGetter?.baseUri != null) {
            imageGetter.baseUri?.resolve(urlString)?.toURL()
        } else {
            URI.create(urlString).toURL()
        }
        return url?.content as? InputStream
    }

    private fun fetchFromFile(file: File): FileInputStream {
        return FileInputStream(file)
    }
}