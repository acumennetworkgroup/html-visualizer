package org.htmlparser.core.rendering

import android.app.Application
import mozilla.components.browser.engine.gecko.GeckoEngine
import mozilla.components.browser.engine.gecko.GeckoEngineView
import mozilla.components.concept.engine.DefaultSettings
import mozilla.components.concept.engine.EngineSession
import org.mozilla.geckoview.GeckoRuntime

class RenderEngine(private val applicationContext: Application) {

    fun bindSessionToRenderView(view: GeckoEngineView): EngineSession {
        val browserSession = createSession()
        view.render(browserSession)
        return browserSession
    }

    // Create default settings (optional) and enable tracking protection for all future sessions.
    private val defaultSettings by lazy {
        DefaultSettings().apply {
            trackingProtectionPolicy = EngineSession.TrackingProtectionPolicy.recommended()
        }
    }

    // Create and initialize a Gecko runtime with the default settings.
    private val runtime: GeckoRuntime by lazy { GeckoRuntime.getDefault(applicationContext) }

    // Create an  engine instance to be used by the browser view component.
    private val engine by lazy {
        GeckoEngine(
            applicationContext,
            defaultSettings,
            runtime
        )
    }

    private fun createSession(): EngineSession {
        return engine.createSession(false, null)
    }
}