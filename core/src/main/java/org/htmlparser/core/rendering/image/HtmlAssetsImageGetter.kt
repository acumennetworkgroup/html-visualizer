package org.htmlparser.core.rendering.image

import android.content.Context
import android.graphics.drawable.Drawable
import android.text.Html.ImageGetter
import android.util.Log
import android.widget.TextView
import org.htmlparser.core.rendering.HtmlView
import java.io.IOException

/**
 * Assets Image Getter
 * Load image from assets folder
 *
 */
class HtmlAssetsImageGetter : ImageGetter {
    private val context: Context

    constructor(context: Context) {
        this.context = context
    }

    constructor(textView: TextView) {
        context = textView.context
    }

    override fun getDrawable(source: String): Drawable? {
        return try {
            val inputStream = context.assets.open(source)
            val d = Drawable.createFromStream(inputStream, null)
            d.setBounds(0, 0, d.intrinsicWidth, d.intrinsicHeight)
            d
        } catch (e: IOException) {
            // prevent a crash if the resource still can't be found
            Log.e(HtmlView.TAG, "source could not be found: $source")
            null
        }
    }
}