package org.htmlparser.core.rendering.image

import android.graphics.drawable.Drawable
import android.text.Html.ImageGetter
import android.widget.TextView
import androidx.core.content.ContextCompat
import org.htmlparser.core.caching.ImageProcessing.getImageSize
import org.htmlparser.core.rendering.ImageGetterCoroutinesTask
import java.net.URI

class HtmlHttpImageGetter : ImageGetter {
    var container: TextView

    var baseUri: URI? = null
        private set
    private var matchParentWidth = false
    private var placeHolder = 0
    private var compressImage = false
    private var qualityImage = 50

    constructor(textView: TextView) {
        container = textView
        matchParentWidth = false
    }

    constructor(textView: TextView, baseUrl: String?) {
        container = textView
        if (baseUrl != null) {
            baseUri = URI.create(baseUrl)
        }
    }

    constructor(textView: TextView, baseUrl: String?, matchParentWidth: Boolean) : this(
        textView,
        baseUrl,
        0,
        matchParentWidth
    )

    constructor(
        textView: TextView, baseUrl: String?, placeHolder: Int,
        matchParentWidth: Boolean
    ) {
        container = textView
        this.placeHolder = placeHolder
        this.matchParentWidth = matchParentWidth
        if (baseUrl != null) {
            baseUri = URI.create(baseUrl)
        }
    }

    @JvmOverloads
    fun enableCompressImage(enable: Boolean, quality: Int = 50) {
        compressImage = enable
        qualityImage = quality
    }

    override fun getDrawable(source: String): Drawable {
        val urlDrawable = UrlDrawable()
        if (placeHolder != 0) {

            val placeDrawable = ContextCompat.getDrawable(container.context, placeHolder)

            val imageSize = getImageSize(source)
            val width = imageSize.width
            val height = imageSize.height

            placeDrawable!!.setBounds(0, 0, width, height)
            urlDrawable.setBounds(
                0, 0, placeDrawable.intrinsicWidth,
                placeDrawable.intrinsicHeight
            )
            urlDrawable.drawable = placeDrawable
        }
        // get the actual source
        val asyncTask = ImageGetterCoroutinesTask(
            urlDrawable, this, container,
            matchParentWidth, compressImage, qualityImage
        )
        asyncTask.execute(source)

        // return reference to URLDrawable which will asynchronously load the image specified in the src tag
        return urlDrawable
    }
}