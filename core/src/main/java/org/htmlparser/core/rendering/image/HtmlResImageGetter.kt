package org.htmlparser.core.rendering.image

import android.content.Context
import android.graphics.drawable.Drawable
import android.text.Html.ImageGetter
import android.util.Log
import androidx.core.content.ContextCompat
import org.htmlparser.core.rendering.HtmlView

class HtmlResImageGetter(private val context: Context) : ImageGetter {
    override fun getDrawable(source: String): Drawable? {
        var id = context.resources.getIdentifier(source, "drawable", context.packageName)
        if (id == 0) {
            // the drawable resource wasn't found in our package, maybe it is a stock android drawable?
            id = context.resources.getIdentifier(source, "drawable", "android")
        }
        return if (id == 0) {
            // prevent a crash if the resource still can't be found
            Log.e(HtmlView.TAG, "source could not be found: $source")
            null
        } else {
            val d: Drawable? = ContextCompat.getDrawable(context, id)
            d?.setBounds(0, 0, d.intrinsicWidth, d.intrinsicHeight)
            d
        }
    }
}