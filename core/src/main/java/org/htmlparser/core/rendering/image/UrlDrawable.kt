package org.htmlparser.core.rendering.image

import android.graphics.Canvas
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable

@SuppressWarnings("deprecation")
class UrlDrawable(var drawable: Drawable? = null) : BitmapDrawable() {

    override fun draw(canvas: Canvas) {
        // override the draw to facilitate refresh function later
        if (drawable != null) {
            drawable!!.draw(canvas)
        }
    }
}