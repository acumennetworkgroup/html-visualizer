package org.htmlparser.core.rendering.table

import android.text.style.ClickableSpan

/**
 * This span defines what should happen if a table is clicked. This abstract class is defined so
 * that applications can access the raw table HTML and do whatever they'd like to render it (e.g.
 * show it in a WebView).
 */
abstract class ClickableTableSpan : ClickableSpan() {
    var tableHtml: String? = null

    // This sucks, but we need this so that each table can get its own ClickableTableSpan.
    // Otherwise, we end up removing the clicking from earlier tables.
    abstract fun newInstance(): ClickableTableSpan?
}