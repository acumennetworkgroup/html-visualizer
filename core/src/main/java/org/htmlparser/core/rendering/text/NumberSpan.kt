package org.htmlparser.core.rendering.text

import android.graphics.Canvas
import android.graphics.Paint
import android.os.Parcel
import android.text.Layout
import android.text.Spanned
import android.text.style.BulletSpan

/**
 * Class to use Numbered Lists in TextViews.
 * The span works the same as [BulletSpan] and all lines of the entry have
 * the same leading margin.
 */
class NumberSpan : BulletSpan {
    private val mNumberGapWidth: Int
    private val mNumber: String?

    constructor(gapWidth: Int, number: Int) : super() {
        mNumberGapWidth = gapWidth
        mNumber = Integer.toString(number) + "."
    }

    constructor(number: Int) : this(STANDARD_GAP_WIDTH, number)
    constructor(src: Parcel) : super(src) {
        mNumberGapWidth = src.readInt()
        mNumber = src.readString()
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        super.writeToParcel(dest, flags)
        dest.writeInt(mNumberGapWidth)
        dest.writeString(mNumber)
    }

    override fun getLeadingMargin(first: Boolean): Int {
        return 2 * STANDARD_GAP_WIDTH + mNumberGapWidth
    }

    override fun drawLeadingMargin(
        c: Canvas, p: Paint, x: Int, dir: Int,
        top: Int, baseline: Int, bottom: Int, text: CharSequence,
        start: Int, end: Int, first: Boolean, l: Layout?
    ) {
        if ((text as Spanned).getSpanStart(this) == start) {
            val style = p.style
            p.style = Paint.Style.FILL
            if (c.isHardwareAccelerated) {
                c.save()
                c.drawText(mNumber!!, (x + dir).toFloat(), baseline.toFloat(), p)
                c.restore()
            } else {
                c.drawText(mNumber!!, (x + dir).toFloat(), (top + bottom) / 2.0f, p)
            }
            p.style = style
        }
    }

    companion object {
        const val STANDARD_GAP_WIDTH = 10
    }
}